<?php
	
  //Buffer larger content areas like the main page content
  ob_start();

?>

<div class="content">
	<?php
	if (isset($_GET['race_id']) ) {
	
		$race_id = $_GET['race_id'];
		
		include_once "function/function_race_info.php";
		
		$race_array = get_race_data($race_id); // returns an array of race data
		$contestant_array = get_contestant_data($race_id); //returns a 2D array of contestant data
		
		echo 
			$race_array['date'] ."<br/><b>".
			$race_array['venue']  ." ".
			$race_array['venue_code'] ." R".
			$race_array['race_num']  ." ".
			$race_array['distance']  ." metres </b><br/>";
			
			
		// Call $row by reference with &	
		foreach ($contestant_array as &$row) {
			
			//----Calculate speed-----------------------------------
			if ($row['finish_time1'] != 0) {
				//Adding a new column to $contestant_array 
				$row['speed1'] = round($row['distance1']/$row['finish_time1'], 2); // speed = distance/time
			} else {
				//Adding a new column to $contestant_array 
				$row['speed1'] = 0;
			}
			
			if ($row['finish_time2'] != 0) {
				//Adding a new column to $contestant_array 
				$row['speed2'] = round($row['distance2']/$row['finish_time2'], 2); // speed = distance/time
			} else {
				$row['speed2'] = 0;
			}
			
			if ($row['finish_time3'] != 0) {
				//Adding a new column to $contestant_array 
				$row['speed3'] = round($row['distance3']/$row['finish_time3'], 2); // speed = distance/time
			} else {
				$row['speed3'] = 0;
			}
			
			if ($row['finish_time4'] != 0) {
				//Adding a new column to $contestant_array 
				$row['speed4'] = round($row['distance4']/$row['finish_time4'], 2); // speed = distance/time
			} else {
				$row['speed4'] = 0;
			}
			
			if ($row['finish_time5'] != 0) {
				//Adding a new column to $contestant_array 
				$row['speed5'] = round($row['distance5']/$row['finish_time5'], 2); // speed = distance/time
			} else {
				$row['speed5'] = 0;
			}
			
			
			//----Calculate speed using adjusted time-----------------------------------
			if ($row['adjusted_time1'] != 0) {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed1'] = round($row['distance1']/$row['adjusted_time1'], 2);
			} else {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed1'] = 0;
			}
			
			if ($row['adjusted_time2'] != 0) {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed2'] = round($row['distance2']/$row['adjusted_time2'], 2);
			} else {
				$row['adjustedSpeed2'] = 0;
			}
			
			if ($row['adjusted_time3'] != 0) {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed3'] = round($row['distance3']/$row['adjusted_time3'], 2);
			} else {
				$row['adjustedSpeed3'] = 0;
			}
			
			if ($row['adjusted_time4'] != 0) {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed4'] = round($row['distance4']/$row['adjusted_time4'], 2);
			} else {
				$row['adjustedSpeed4'] = 0;
			}
			
			if ($row['adjusted_time5'] != 0) {
				//Adding a new column to $contestant_array 
				$row['adjustedSpeed5'] = round($row['distance5']/$row['adjusted_time5'], 2);
			} else {
				$row['adjustedSpeed5'] = 0;
			}
			
			
			
			$distance_array = array($row['distance1'],$row['distance2'],$row['distance3'],$row['distance4'],$row['distance5']);
			$speed_array = array($row['speed1'],$row['speed2'],$row['speed3'],$row['speed4'],$row['speed5']);
			$adjusted_speed_array = array($row['adjustedSpeed1'],$row['adjustedSpeed2'],$row['adjustedSpeed3'],$row['adjustedSpeed4'],$row['adjustedSpeed5']);
									
			//---- which distance that contestant has ran is the closest to the distance of current race? -----			
			$closest_distance_to_race_distance = $row['distance1'];
			$distance_diff = abs($race_array['distance'] - $row['distance1']);
			
			for ($i = 1; $i < count($distance_array); $i++) {
				
				// the smaller the difference between the two distances, the closer they are to each other 
				$temp_distance_diff = abs($race_array['distance'] - $distance_array[$i]);
				
				if ($temp_distance_diff < $distance_diff) {
					$distance_diff = $temp_distance_diff;
					$closest_distance_to_race_distance = $distance_array[$i];
				}
			
			}
			
			$row['closest_distance_to_race_distance'] = $closest_distance_to_race_distance;
			
			// find the best speed of contestant at the current race distance
			//(or use closest distance if contestant has not ran the distance of current race before )		
			$best_speed_array = get_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance );
			$row['best_speed_at_race_distance']= $best_speed_array['speed'];
			$best_speed_index = $best_speed_array['index'];
			
			//find best adjusted speed of contestant
			$best_adjusted_speed = get_best_speed($distance_array, $adjusted_speed_array, $closest_distance_to_race_distance );
			$row['best_adjusted_speed']= $best_adjusted_speed['speed'];
			$best_adjusted_speed_index = $best_adjusted_speed['index'];
			
			// Second best speed of contestant						
			$best_speed_array_2 = get_2nd_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance, $best_speed_index );
			$row['best_speed_2_at_race_distance']= $best_speed_array_2['speed'];
			$best_speed_index_2 = $best_speed_array_2['index'];
			
			//find 2nd best adjusted speed of contestant
			$best_adjusted_speed_2 = get_2nd_best_speed($distance_array, $adjusted_speed_array, $closest_distance_to_race_distance , $best_adjusted_speed_index);
			$row['best_adjusted_speed_2']= $best_adjusted_speed_2['speed'];
			$best_adjusted_speed_index_2 = $best_adjusted_speed_2['index'];
			
			// Third best speed of contestant
			$best_speed_3  = 0;
			$best_speed_array_3 = get_3rd_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance, $best_speed_index, $best_speed_index_2 );			
			$row['best_speed_3_at_race_distance']= $best_speed_array_3['speed'];
			
			//find 3rd best adjusted speed of contestant
			$best_adjusted_speed_3 = get_3rd_best_speed($distance_array, $adjusted_speed_array, $closest_distance_to_race_distance , $best_adjusted_speed_index, $best_adjusted_speed_index_2 );
			$row['best_adjusted_speed_3']= $best_adjusted_speed_3['speed'];

			
			
			
			// Average of best 3 speeds at race distance			
			$row['avg_top_3_speed'] = get_avg_top_3_speed( $row['best_speed_at_race_distance'], 
					$row['best_speed_2_at_race_distance'], $row['best_speed_3_at_race_distance'] );
					
					
			// Average of best 3 adjusted speeds at race distance		
			$row['avg_top_3_adjusted_speed'] = get_avg_top_3_speed( $row['best_adjusted_speed'], 
					$row['best_adjusted_speed_2'], $row['best_adjusted_speed_3'] );
			
			
			//---Calculate average speed of contestant-----------------
			$speed_counter = 0;
			$total_speed = 0;
			
			for ($i = 0; $i < count($speed_array); $i++) {
				if ($speed_array[$i] != 0) {
					$total_speed = $total_speed + $speed_array[$i];
					$speed_counter++;
				}
			}
			
			$row['average_speed'] = round($total_speed / $speed_counter, 2);
			
			
			

		} // foreach
		
		
				
			
		
	} // if isset(GET)
		
		
		
		
		
		function get_avg_top_3_speed($best_speed, $best_speed_2, $best_speed_3) {
			
			$avg_top_3_speed = 0;
			
			if ($best_speed_3 != 0 && $best_speed_2 != 0) {
				$avg_top_3_speed = round(($best_speed + $best_speed_2 + $best_speed_3)/3, 2);
				
			} else if ($best_speed_3 == 0 && $best_speed_2 != 0) {
				$avg_top_3_speed = round(($best_speed + $best_speed_2)/2, 2);
				
			} else {
				$avg_top_3_speed = $best_speed;
			}
			
			return $avg_top_3_speed;
		}
		
		
		
		
		function get_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance) {
			$best_speed = array();
			$best_speed['speed'] = 0;
			
			for ($i = 0; $i < count($distance_array); $i++) {
							
				if ($distance_array[$i] == $closest_distance_to_race_distance
					&& $speed_array[$i] > $best_speed['speed']) {
					$best_speed['speed'] = $speed_array[$i];
					$best_speed['index'] = $i;
				}
			}
			
			return $best_speed;
		
		}
		
		
		function get_2nd_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance, $best_speed_index ) {
			$best_speed = array();
			$best_speed['speed'] = 0;
			
			for ($i = 0; $i < count($distance_array); $i++) {
							
				if ($distance_array[$i] == $closest_distance_to_race_distance
					&& $speed_array[$i] > $best_speed['speed'] 
					&& $i != $best_speed_index) {
					$best_speed['speed'] = $speed_array[$i];
					$best_speed['index'] = $i;
				}
			}
			
			return $best_speed;
		
		}
		
		
		function get_3rd_best_speed($distance_array, $speed_array, $closest_distance_to_race_distance, 
				$best_speed_index, $best_speed_index_2 ) {
			$best_speed = array();
			$best_speed['speed'] = 0;
			
			for ($i = 0; $i < count($distance_array); $i++) {
							
				if ($distance_array[$i] == $closest_distance_to_race_distance
					&& $speed_array[$i] > $best_speed['speed']
					&& $i != $best_speed_index
					&& $i != $best_speed_index_2) {
					$best_speed['speed'] = $speed_array[$i];
					$best_speed['index'] = $i;
				}
			}
			
			return $best_speed;
		
		}
		
	
		function syle_if_equals_best_speed($speed, $best_speed, $distance_ran, $race_distance) {
			if ($speed == $best_speed && $distance_ran == $race_distance) {
				$styling = "red";
			} else if ($speed == 0){
				$styling = "white";
			} else {
				$styling = "";
			}
			
			return $styling;
		
		}
		
		function syle_if_equals_any_best_3_speed($speed, $best_speed, $best_speed_2, $best_speed_3,$distance_ran, $race_distance) {
			if ( ($speed == $best_speed || $speed == $best_speed_2 || $speed == $best_speed_3 ) 
				&& $distance_ran == $race_distance) {
				$styling = "red";
			} else if ($speed == 0){
				$styling = "white";
			} else {
				$styling = "";
			}
			
			return $styling;
		
		}
	?>
	
	
	
	
	
	<div>
		<h2>Ranked by Best Speed in <?php echo $race_array['distance']?> metres</h2>
		
		<?php
			include_once "function/class_sort_array.php"; // include FieldSorter class
		
			// sort $contestant_array by best_speed_at_race_distance column in descending order
			$sorter = new FieldSorter('best_speed_at_race_distance');    
			usort($contestant_array, array($sorter, "cmp")); 
			
			$styling = "";
			
			foreach ($contestant_array as &$row) {
			
				echo "
				<div class='column_div'> 
					<b>".
					$row['contestant_num']
					."</b> <br/>  
					<div class='runner_name_div'>" . 
					$row['name'] ."</div><br/>";
					
				
				$styling = syle_if_equals_best_speed($row['speed1'], $row['best_speed_at_race_distance'],$row['distance1'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time1']." (". $row['distance1'] ."m) ". $row['position1']. "</span>
					<br/>". $row['speed1'] ."m/s <hr/>";
				
				
				$styling = syle_if_equals_best_speed($row['speed2'], $row['best_speed_at_race_distance'],$row['distance2'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time2']." (". $row['distance2'] ."m) ". $row['position2']. "</span>
					<br/>". $row['speed2'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['speed3'], $row['best_speed_at_race_distance'], $row['distance3'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time3']." (". $row['distance3'] ."m) ". $row['position3']. "</span>
					<br/>". $row['speed3'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['speed4'], $row['best_speed_at_race_distance'], $row['distance4'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time4']." (". $row['distance4'] ."m) ". $row['position4']. "</span>
					<br/>". $row['speed4'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['speed5'], $row['best_speed_at_race_distance'], $row['distance5'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time5']." (". $row['distance5'] ."m) ". $row['position5']. "</span>
					<br/>". $row['speed5'] ."m/s <hr/>";
					
				
				echo
				"</div>";
			
			} // foreach
						
		?>
	
	</div>
	
	
	<div style='clear:both'></div>
	<hr/>
	
	<div>
		<h2>Ranked by Average of Top 3 Speed in <?php echo $race_array['distance']?> metres</h2>
		
		<?php
					
			// sort $contestant_array by avg_top_3_speed column in descending order
			$sorter = new FieldSorter('avg_top_3_speed');    
			usort($contestant_array, array($sorter, "cmp")); 
			
					
			foreach ($contestant_array as &$row) {
			
				echo "
				<div class='column_div'> 
					<b>".
					$row['contestant_num']
					."</b> <br/>" .
					$row['avg_top_3_speed'] ."m/s <hr/>";
					
				
				$styling = syle_if_equals_any_best_3_speed($row['speed1'], $row['best_speed_at_race_distance'],
							$row['best_speed_2_at_race_distance'],$row['best_speed_3_at_race_distance'],
							$row['distance1'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time1']." (". $row['distance1'] ."m) ". $row['position1']. "</span>
					<br/>". $row['speed1'] ."m/s <hr/>";
				
				
				$styling = syle_if_equals_any_best_3_speed($row['speed2'], $row['best_speed_at_race_distance'],
							$row['best_speed_2_at_race_distance'],$row['best_speed_3_at_race_distance'],
							$row['distance2'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time2']." (". $row['distance2'] ."m) ". $row['position2']. "</span>
					<br/>". $row['speed2'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['speed3'], $row['best_speed_at_race_distance'],
							$row['best_speed_2_at_race_distance'],$row['best_speed_3_at_race_distance'],
							$row['distance3'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time3']." (". $row['distance3'] ."m) ". $row['position3']. "</span>
					<br/>". $row['speed3'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['speed4'], $row['best_speed_at_race_distance'], 
							$row['best_speed_2_at_race_distance'],$row['best_speed_3_at_race_distance'],
							$row['distance4'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time4']." (". $row['distance4'] ."m) ". $row['position4']. "</span>
					<br/>". $row['speed4'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['speed5'], $row['best_speed_at_race_distance'], 
							$row['best_speed_2_at_race_distance'],$row['best_speed_3_at_race_distance'],
							$row['distance5'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['finish_time5']." (". $row['distance5'] ."m) ". $row['position5']. "</span>
					<br/>". $row['speed5'] ."m/s <hr/>";
					
				
				echo
				"</div>";
			
			} // foreach
						
		?>
	
	</div>
	
	
		
	
	<div style='clear:both'></div>
	<hr/>
	
	<div>
		<h2>Ranked by Average Speed</h2>
		
		<?php
					
			// sort $contestant_array by average_speed column in descending order
			$sorter = new FieldSorter('average_speed');    
			usort($contestant_array, array($sorter, "cmp")); 
			
					
			foreach ($contestant_array as &$row) {
			
				echo "
				<div class='column_div'> 
					<b>".
					$row['contestant_num']
					."</b> <br/>" ;
					
				echo "
					<span>" .$row['average_speed']."m/s ";
					
				echo
				"</div>";
			
			} // foreach
						
		?>
	
	</div>
	
	
	
	
	<div style='clear:both'></div>
	<hr/>
	
	<div>
		<h2>Ranked by Average of Top 3 Adjusted Speed in <?php echo $race_array['distance']?> metres</h2>
		
		<?php
					
			// sort $contestant_array by avg_top_3_adjusted_speed column in descending order
			$sorter = new FieldSorter('avg_top_3_adjusted_speed');    
			usort($contestant_array, array($sorter, "cmp")); 
			
					
			foreach ($contestant_array as &$row) {
			
				echo "
				<div class='column_div'> 
					<b>".
					$row['contestant_num'] 
					."</b> <br/>".
					$row['avg_top_3_adjusted_speed'] ."m/s <hr/>" ;
					
				
				$styling = syle_if_equals_any_best_3_speed($row['adjustedSpeed1'], $row['best_adjusted_speed'],
							$row['best_adjusted_speed_2'],$row['best_adjusted_speed_3'],
							$row['distance1'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time1']." (". $row['distance1'] ."m) ". $row['position1']. "</span>
					<br/>". $row['adjustedSpeed1'] ."m/s <hr/>";
				
				
				$styling = syle_if_equals_any_best_3_speed($row['adjustedSpeed2'], $row['best_adjusted_speed'],
							$row['best_adjusted_speed_2'],$row['best_adjusted_speed_3'],
							$row['distance2'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time2']." (". $row['distance2'] ."m) ". $row['position2']. "</span>
					<br/>". $row['adjustedSpeed2'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['adjustedSpeed3'], $row['best_adjusted_speed'],
							$row['best_adjusted_speed_2'],$row['best_adjusted_speed_3'],
							$row['distance3'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time3']." (". $row['distance3'] ."m) ". $row['position3']. "</span>
					<br/>". $row['adjustedSpeed3'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['adjustedSpeed4'], $row['best_adjusted_speed'],
							$row['best_adjusted_speed_2'],$row['best_adjusted_speed_3'],
							$row['distance4'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time4']." (". $row['distance4'] ."m) ". $row['position4']. "</span>
					<br/>". $row['adjustedSpeed4'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_any_best_3_speed($row['adjustedSpeed5'], $row['best_adjusted_speed'],
							$row['best_adjusted_speed_2'],$row['best_adjusted_speed_3'],
							$row['distance5'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time5']." (". $row['distance5'] ."m) ". $row['position5']. "</span>
					<br/>". $row['adjustedSpeed5'] ."m/s <hr/>";
					
				
				echo
				"</div>";
			
			} // foreach
						
		?>
	
	</div>
	
	
	<div style='clear:both'></div>
	<hr/>
	
	
	<div>
		<h2>Ranked by Best Adjusted Speed in <?php echo $race_array['distance']?> metres</h2>
		
		<?php
			
		
			// sort $contestant_array by best_adjusted_speed column in descending order
			$sorter = new FieldSorter('best_adjusted_speed');    
			usort($contestant_array, array($sorter, "cmp")); 
			
			$styling = "";
			
			foreach ($contestant_array as &$row) {
			
				echo "
				<div class='column_div'> 
					<b>".
					$row['contestant_num']
					."</b> <br/>" ;
					
				
				$styling = syle_if_equals_best_speed($row['adjustedSpeed1'], $row['best_adjusted_speed'],$row['distance1'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time1']." (". $row['distance1'] ."m) ". $row['position1']. "</span>
					<br/>". $row['adjustedSpeed1'] ."m/s <hr/>";
				
				
				$styling = syle_if_equals_best_speed($row['adjustedSpeed2'], $row['best_adjusted_speed'],$row['distance2'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time2']." (". $row['distance2'] ."m) ". $row['position2']. "</span>
					<br/>". $row['adjustedSpeed2'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['adjustedSpeed3'], $row['best_adjusted_speed'], $row['distance3'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time3']." (". $row['distance3'] ."m) ". $row['position3']. "</span>
					<br/>". $row['adjustedSpeed3'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['adjustedSpeed4'], $row['best_adjusted_speed'], $row['distance4'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time4']." (". $row['distance4'] ."m) ". $row['position4']. "</span>
					<br/>". $row['adjustedSpeed4'] ."m/s <hr/>";
					
					
				$styling = syle_if_equals_best_speed($row['adjustedSpeed5'], $row['best_adjusted_speed'], $row['distance5'],$row['closest_distance_to_race_distance']);
				echo "
					<span class='$styling'>" .$row['adjusted_time5']." (". $row['distance5'] ."m) ". $row['position5']. "</span>
					<br/>". $row['adjustedSpeed5'] ."m/s <hr/>";
					
				
				echo
				"</div>";
			
			} // foreach
						
		?>
	
	</div>
	
	
		
</div> <!-- class=content -->

	
		
		

<?php
	  //Assign all Page Specific variables
	  $MainContent = ob_get_contents();
	  ob_end_clean();
	  $title = "Race Analysis";
	  
	  
	  
	  $rightContent = " 
		
	  ";
	  
	  

	  
	  //Apply the template
	  include("masterpage.php");
	?>