<?php
		
  //retrieve file containing database connection
	include "dbconn.php";
	
	if (isset($_POST['raceNum']))
	{
		$raceDate = date("Y-m-d", strtotime( $_POST['racedate'] ) );
		$venueName = trim($_POST['venueName']);
		$venueCode = strtoupper( trim($_POST['venueCode']) );
		$raceNum = $_POST['raceNum'];
		$distance = $_POST['distance'];
		         
		// prepare and bind
		$stmt = $con->prepare("INSERT INTO race (race_date,venue_name,venue_code,race_num,distance) VALUES (?,?,?,?,?)");
		$stmt->bind_param("sssii", $raceDate,$venueName,$venueCode,$raceNum,$distance); //s = string, d = double, i = integer

		// execute
		if ($stmt->execute()) {
			echo "
			<script>
				window.location.href = 'contestants_form.php?race_date=$raceDate&venue_name=$venueName&venue_code=$venueCode&race_num=$raceNum&distance=$distance';
			</script>
			";			
		} else { echo "Error: $venueName Race $raceNum not added";}
		
		$stmt->close();
		$con->close();

           
		
	
	} // isset($_POST['title'])
	
?> 