<?php
	include_once "function_race_info.php";
	
	include_once "domain.php"; // returns $mydomain
	
	$race = get_recently_added_race(); // this function returns a 2D array of recently added races
	
	echo "<b>Recently Added</b><br/>";
	
	$previous_row_date;
	
	for ($row = 0; $row < count($race); $row++) {
	
		$race_id = $race[$row]['race_id'];
		$raceDate = $race[$row]['date'];
		$venueName = $race[$row]['venue'];
		$venueCode = $race[$row]['venue_code'];
		$raceNum = $race[$row]['race_num'];
		$distance = $race[$row]['distance'];
		
		
		if ($row == 0) { // echo date is this is first row
			echo "$raceDate"; 
		} else if ($previous_row_date != $raceDate) { // else if date is different from date of previous row
			echo "<br/><br/> $raceDate";
		}
		
		echo "
		<br/>
		<a href='$mydomain/race_analysis.php?race_id=$race_id'>
				 $venueName $venueCode R$raceNum</a>";
	
		
		$previous_row_date = $raceDate;
	}
	
		
?> 