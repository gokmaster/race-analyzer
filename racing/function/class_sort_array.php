<?php
			
	// this class sorts a multi-dimensional array by a user defined field/column
	class FieldSorter {
		public $field;

		function __construct($field) {
			$this->field = $field;
		}

		function cmp($a, $b) {
			if ($a[$this->field] == $b[$this->field]) return 0;
			return ($a[$this->field] < $b[$this->field]) ? 1 : -1; //Descending order. To reverse the order, change the < to >
		}
	}
	
	 
?>