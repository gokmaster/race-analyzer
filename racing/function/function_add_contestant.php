<?php
	
	function add_contestant_to_race($race_id,$contestantNum,$contestantName,
					$finishTime1,$finishTime2,$finishTime3,$finishTime4,$finishTime5,
					$distance1,$distance2,$distance3,$distance4,$distance5,
					$position1,$position2,$position3,$position4,$position5,
					$adjustedTime1,$adjustedTime2,$adjustedTime3,$adjustedTime4,$adjustedTime5) 
	{
					
		include "dbconn.php";
		include_once "function_race_info.php";
		
		// if contestant not already in race
		if (!is_contestant_in_race($race_id,$contestantNum) ) {
		         
			// prepare and bind
			$stmt = $con->prepare("INSERT INTO contestant (race_id,contestant_num,name,
				finish_time1,finish_time2,finish_time3,finish_time4,finish_time5,
				distance1,distance2,distance3,distance4,distance5,
				position1,position2,position3,position4,position5,
				adjusted_time1,adjusted_time2,adjusted_time3,adjusted_time4,adjusted_time5
			) VALUES (?,?,?,  ?,?,?,?,?,  ?,?,?,?,?,   ?,?,?,?,?,   ?,?,?,?,?)");
			$stmt->bind_param("iisdddddiiiiiiiiiiddddd", 
					$race_id,$contestantNum,$contestantName,
					$finishTime1,$finishTime2,$finishTime3,$finishTime4,$finishTime5,
					$distance1,$distance2,$distance3,$distance4,$distance5,
					$position1,$position2,$position3,$position4,$position5,
					$adjustedTime1,$adjustedTime2,$adjustedTime3,$adjustedTime4,$adjustedTime5); //s = string, d = double, i = integer

			// execute
			if ($stmt->execute()) {
				echo "
				<br/> Added Contestant $contestantNum
				";			
			} else { echo "Error: Contestant $contestantNum could not be added";}
			
			$stmt->close();
			$con->close();
		
		
		} else {
			echo "<br/><span class='red'>Contestant $contestantNum is already in this race. Please enter different contestant number.</span>";
		}
		
	}
	
?> 