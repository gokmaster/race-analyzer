<?php
			
	function get_race_id($race_date,$venue_code,$race_num)
	{	
		//retrieve file containing database connection
		include "dbconn.php";
		
		// prepare and bind
		$stmt2 = mysqli_prepare($con, "SELECT MAX(race_id) FROM race WHERE race_date=? AND venue_code=? AND race_num=? ");
		mysqli_stmt_bind_param($stmt2, "ssi", $race_date,$venue_code,$race_num); //s = string, d = double, i = integer

		// execute query
		mysqli_stmt_execute($stmt2);
				
		//bind the result of that query to variables
		mysqli_stmt_bind_result($stmt2, $raceID);
				
		mysqli_stmt_fetch($stmt2);
		
		mysqli_stmt_close($stmt2);
		
		return $raceID;
	} 

//--------------------------------------------------	

	function get_recently_added_race()
	{	
		//retrieve file containing database connection
		include "dbconn.php";
		
		// prepare and bind
		$stmt2 = mysqli_prepare($con, "SELECT race_id,race_date,venue_name,venue_code,race_num,distance FROM race ORDER BY race_id DESC LIMIT 60 ");
		
		// execute query
		mysqli_stmt_execute($stmt2);
				
		//bind the result of that query to variables
		mysqli_stmt_bind_result($stmt2, $raceID, $date, $venue,$venueCode,$raceNum,$distance);
		
		$race_array = array();
		$count = 0;
				
		while (mysqli_stmt_fetch($stmt2)) {
			
			$race_array[$count]= array();
			$race_array[$count]['race_id'] = $raceID;
			$race_array[$count]['date'] = $date;
			$race_array[$count]['venue'] = $venue;
			$race_array[$count]['venue_code'] = $venueCode;
			$race_array[$count]['race_num'] = $raceNum;
			$race_array[$count]['distance'] = $distance;
			
			$count++;
		}
		
		mysqli_stmt_close($stmt2);
		
		return $race_array;
	} 
	
	//--------------------------------------------------	

	function get_race_data($race_id)
	{	
		//retrieve file containing database connection
		include "dbconn.php";
		
		// prepare and bind
		$stmt2 = mysqli_prepare($con, "SELECT race_date,venue_name,venue_code,race_num,distance FROM race WHERE race_id = ? ");
		mysqli_stmt_bind_param($stmt2, "i", $race_id); //s = string, d = double, i = integer
		
		// execute query
		mysqli_stmt_execute($stmt2);
				
		//bind the result of that query to variables
		mysqli_stmt_bind_result($stmt2, $date, $venue,$venueCode,$raceNum,$distance);
		
		$race_array = array();
						
		while (mysqli_stmt_fetch($stmt2)) {
			
			$race_array['date'] = $date;
			$race_array['venue'] = $venue;
			$race_array['venue_code'] = $venueCode;
			$race_array['race_num'] = $raceNum;
			$race_array['distance'] = $distance;
			
		}
		
		mysqli_stmt_close($stmt2);
		
		return $race_array;
	} 
	
	
	
	//--------------------------------------------------------------
	
	function get_contestant_data($race_id)
	{	
		//retrieve file containing database connection
		include "dbconn.php";
		
		// prepare and bind
		$stmt2 = mysqli_prepare($con, "SELECT contestant_num,name,
			finish_time1,finish_time2,finish_time3,finish_time4,finish_time5,
			distance1,distance2,distance3,distance4,distance5,
			position1,position2,position3,position4,position5,
			adjusted_time1,adjusted_time2,adjusted_time3,adjusted_time4,adjusted_time5
			FROM contestant WHERE race_id = ? ");
		mysqli_stmt_bind_param($stmt2, "i", $race_id); //s = string, d = double, i = integer
		
		// execute query
		mysqli_stmt_execute($stmt2);
				
		//bind the result of that query to variables
		mysqli_stmt_bind_result($stmt2, $contestant_num,$name,
			$finish_time1,$finish_time2,$finish_time3,$finish_time4,$finish_time5,
			$distance1,$distance2,$distance3,$distance4,$distance5,
			$position1,$position2,$position3,$position4,$position5,
			$adjusted_time1,$adjusted_time2,$adjusted_time3,$adjusted_time4,$adjusted_time5);
		
		$contestant_array = array();
		$count = 0;
						
		while (mysqli_stmt_fetch($stmt2)) {
			
			$contestant_array[$count]= array();
			$contestant_array[$count]['contestant_num'] = $contestant_num;
			$contestant_array[$count]['name'] = $name;
			$contestant_array[$count]['finish_time1'] = $finish_time1;
			$contestant_array[$count]['finish_time2'] = $finish_time2;
			$contestant_array[$count]['finish_time3'] = $finish_time3;
			$contestant_array[$count]['finish_time4'] = $finish_time4;
			$contestant_array[$count]['finish_time5'] = $finish_time5;
			$contestant_array[$count]['distance1'] = $distance1;
			$contestant_array[$count]['distance2'] = $distance2;
			$contestant_array[$count]['distance3'] = $distance3;
			$contestant_array[$count]['distance4'] = $distance4;
			$contestant_array[$count]['distance5'] = $distance5;
			$contestant_array[$count]['position1'] = $position1;
			$contestant_array[$count]['position2'] = $position2;
			$contestant_array[$count]['position3'] = $position3;
			$contestant_array[$count]['position4'] = $position4;
			$contestant_array[$count]['position5'] = $position5;
			$contestant_array[$count]['adjusted_time1'] = $adjusted_time1;
			$contestant_array[$count]['adjusted_time2'] = $adjusted_time2;
			$contestant_array[$count]['adjusted_time3'] = $adjusted_time3;
			$contestant_array[$count]['adjusted_time4'] = $adjusted_time4;
			$contestant_array[$count]['adjusted_time5'] = $adjusted_time5;
			
			$count++;
		}
		
		mysqli_stmt_close($stmt2);
		
		return $contestant_array;
	} 
	
	//-------------------------------------------------------
	
	function is_contestant_in_race($race_id,$contestant_num)
	{	
		//retrieve file containing database connection
		include "dbconn.php";
		
		// prepare and bind
		$stmt2 = mysqli_prepare($con, "SELECT COUNT(contestant_num) AS num FROM contestant WHERE contestant_num=? AND race_id=? ");
		mysqli_stmt_bind_param($stmt2, "ii", $contestant_num,$race_id); //s = string, d = double, i = integer

		// execute query
		mysqli_stmt_execute($stmt2);
				
		//bind the result of that query to variables
		mysqli_stmt_bind_result($stmt2, $num);
				
		mysqli_stmt_fetch($stmt2);
		
		mysqli_stmt_close($stmt2);
		
		if ($num > 0) {
			return true; 
		} else {
			return false;
		}
		
	} 
	
	
	
	 
?>