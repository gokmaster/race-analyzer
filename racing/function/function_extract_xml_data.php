<?php
	function extract_data_from_xml($filepath, $race_id) {
		
		// get contents of XML file and store into array
		$xml = simplexml_load_file($filepath) or die("Error: Cannot create object");
				
		$grouped_by_contestant = array();
		
		$n = 0;
		$contestant_count = 0;
		$current_contestant_num = 0;
		$races_count = 1; //count of recent races of contestant
		
		foreach($xml->past_race as $recent_race)
		{
			
			
			if ($n == 0) {
				$current_contestant_num = intval($recent_race->contestant_num); // use intval to get integer value
				$grouped_by_contestant[$contestant_count] = array(); 
			} else {
			
				// if a different contestant
				$next_contestant_num = intval($recent_race->contestant_num);
				if ($current_contestant_num != $next_contestant_num) {
				
					$contestant_count++;
					$grouped_by_contestant[$contestant_count] = array(); // new row of contestant data
					$current_contestant_num = $recent_race->contestant_num; // set variable to new contestant
					$races_count = 1; // reset race count back to 1 since this is a different contestant
				} else {
					$races_count++;
				}
			}
			
			
			$grouped_by_contestant[$contestant_count]['contestantNum'] = intval($recent_race->contestant_num);
			$grouped_by_contestant[$contestant_count]['contestantName'] = trim($recent_race->name);
			
			if ($races_count == 1) {
				$grouped_by_contestant[$contestant_count]['finishTime5'] = floatval($recent_race->finish_time); // use floatval to get float value
				$grouped_by_contestant[$contestant_count]['adjust_time5'] = floatval($recent_race->adjusted_time); 
				$grouped_by_contestant[$contestant_count]['distance5'] = intval($recent_race->distance); // use intval to get integer value
				$grouped_by_contestant[$contestant_count]['position5'] = intval($recent_race->finish_position);
			} else if ($races_count == 2) {
				$grouped_by_contestant[$contestant_count]['finishTime4'] = floatval($recent_race->finish_time);
				$grouped_by_contestant[$contestant_count]['adjust_time4'] = floatval($recent_race->adjusted_time); 
				$grouped_by_contestant[$contestant_count]['distance4'] = intval($recent_race->distance); 
				$grouped_by_contestant[$contestant_count]['position4'] = intval($recent_race->finish_position);
			} else if ($races_count == 3) {
				$grouped_by_contestant[$contestant_count]['finishTime3'] = floatval($recent_race->finish_time);
				$grouped_by_contestant[$contestant_count]['adjust_time3'] = floatval($recent_race->adjusted_time); 
				$grouped_by_contestant[$contestant_count]['distance3'] = intval($recent_race->distance); 
				$grouped_by_contestant[$contestant_count]['position3'] = intval($recent_race->finish_position);
			} else if ($races_count == 4) {
				$grouped_by_contestant[$contestant_count]['finishTime2'] = floatval($recent_race->finish_time);
				$grouped_by_contestant[$contestant_count]['adjust_time2'] = floatval($recent_race->adjusted_time); 
				$grouped_by_contestant[$contestant_count]['distance2'] = intval($recent_race->distance); 
				$grouped_by_contestant[$contestant_count]['position2'] = intval($recent_race->finish_position);
			} else if ($races_count == 5) {
				$grouped_by_contestant[$contestant_count]['finishTime1'] = floatval($recent_race->finish_time);
				$grouped_by_contestant[$contestant_count]['adjust_time1'] = floatval($recent_race->adjusted_time); 
				$grouped_by_contestant[$contestant_count]['distance1'] = intval($recent_race->distance); 
				$grouped_by_contestant[$contestant_count]['position1'] = intval($recent_race->finish_position);
			}
			
			$n++;
		}
		
		include_once "function_add_contestant.php";
	
		for($i = 0; $i < count($grouped_by_contestant); $i++)
		{
			add_contestant_to_race($race_id,
				$grouped_by_contestant[$i]['contestantNum'],
				$grouped_by_contestant[$i]['contestantName'],
				$grouped_by_contestant[$i]['finishTime1'],
				$grouped_by_contestant[$i]['finishTime2'],
				$grouped_by_contestant[$i]['finishTime3'],
				$grouped_by_contestant[$i]['finishTime4'],
				$grouped_by_contestant[$i]['finishTime5'],
				$grouped_by_contestant[$i]['distance1'],
				$grouped_by_contestant[$i]['distance2'],
				$grouped_by_contestant[$i]['distance3'],
				$grouped_by_contestant[$i]['distance4'],
				$grouped_by_contestant[$i]['distance5'], 
				$grouped_by_contestant[$i]['position1'],
				$grouped_by_contestant[$i]['position2'],
				$grouped_by_contestant[$i]['position3'],
				$grouped_by_contestant[$i]['position4'],
				$grouped_by_contestant[$i]['position5'],
				$grouped_by_contestant[$i]['adjust_time1'],
				$grouped_by_contestant[$i]['adjust_time2'],
				$grouped_by_contestant[$i]['adjust_time3'],
				$grouped_by_contestant[$i]['adjust_time4'],
				$grouped_by_contestant[$i]['adjust_time5']);
		}
		
			
		
		
		
		
	}


	
	
	
	
?> 