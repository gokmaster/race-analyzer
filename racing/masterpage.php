
<?php
//--change this based where website folder is located

include_once "function/domain.php"; // returns $mydomain

global $head;
global $searchTerm;

?>

<!DOCTYPE html>
<html>

<head>
  <title><?php echo $title ?></title>
  <meta charset="UTF-8">
  
   <!--optimise viewing on mobile devices-->
  <meta name="viewport" content="width=device-width, initial-scale=0.9">
  
	<!--jquery google CDN-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
 
	<!--jquery UI google CDN-->
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	
    <link href="<?php echo $mydomain; ?>/styles/styles.css" rel="Stylesheet" type="text/css" />
    <link href="<?php echo $mydomain; ?>/styles/racing_styles.css" rel="Stylesheet" type="text/css" />
			
	<style type="text/css">
		<?php echo $style ?>
	</style>
	
	<?php
        
        echo $head;
        ?>
	
</head>

<body>
	
<div class="wrapper">
			
        <div class="menu">  
			
			<div class='left_menu_div' >
            
				<li><a href="<?php echo $mydomain; ?>/index.php">Home</a></li>
				<li class='search_bar'>
					<form action="search.php" method="GET">
						<input id='txtSearch' name='txtSearch' type='text' placeholder='Search' value='<?php echo $searchTerm;?>' maxlength='160' >
						<input id='btnSearch' type='image' src='image/icon/search.jpg' alt='Search' disabled>
					</form>
				</li>
				
			</div> <!--left_menu_div-->
			
        </div> <!--menu-->
		
	<div class="adjustdiv" ></div>


	<?php echo $MainContent;?>
	 
	 
	<div class="rightpane">
		<?php include_once "function/recent_added_race.php" ?>
			
	</div><!--rightpane-->	
	
	

    <div style="clear:both;"></div>
    


    
		
	<div id="footer" style="text-align:center;" >
			
			
			
			
		<p>All rights reserved</p>
			 
	  
	</div> <!--footer-->
</div> <!--wrapper -->
	
	
</body>	

<script type="text/javascript" src="js/search-script.js" ></script>

</html>

