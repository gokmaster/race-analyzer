
$(document).ready(function(){
	// When Create Account button is clicked
    $("#btnAddRace").click(function(){
        var racedate =  $("#dateRace").val();
		var venueName = $("#txtVenueName").val();
		var venueCode = $("#txtVenueCode").val();
		var raceNum = $("#txtRaceNum").val();
		var distance = $("#txtDistance").val();
				
		// && represents AND
		if ( racedate != null && racedate != "" 
			&& venueName != null && venueName != ""
			&& venueCode != null && venueCode != ""
			&& raceNum != null && raceNum != ""
			&& distance != null && distance != ""
			) {
				$("#result-div").html("Processing. Please wait...");
				$.post("function/add_race.php", 
						{racedate: racedate, venueName: venueName, venueCode: venueCode, raceNum: raceNum, distance: distance}, 
						function(result){ 
							$("#result-div").html(result); }
				); //$.post
		} else {
			$("#result-div").html("<span class='red'>Please ensure you have entered all fields</span>");
		}
    }); //#btnAddRace
	

	
}); //document.ready
