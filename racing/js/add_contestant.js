
$(document).ready(function(){
	// When Create Account button is clicked
    $("#btnAddContestant").click(function(){
		var race_id =  $("#hiddenRaceID").val();
        var contestantNum =  $("#txtContestantNum").val();
		var contestantName =  $("#txtName").val();
		var finishTime1 =  $("#txtFinishTime1").val();
		var finishTime2 =  $("#txtFinishTime2").val();
		var finishTime3 =  $("#txtFinishTime3").val();
		var finishTime4 =  $("#txtFinishTime4").val();
		var finishTime5 =  $("#txtFinishTime5").val();
		var distance1 =  $("#txtDistance1").val();
		var distance2 =  $("#txtDistance2").val();
		var distance3 =  $("#txtDistance3").val();
		var distance4 =  $("#txtDistance4").val();
		var distance5 =  $("#txtDistance5").val();
						
		// && represents AND
		if ( contestantNum != null && contestantNum != "") {
				$("#result-div").html("Processing. Please wait...");
				$.post("function/add_contestant.php", 
						{race_id: race_id, contestantNum: contestantNum, contestantName: contestantName, 
						finishTime1: finishTime1, finishTime2: finishTime2,  finishTime3: finishTime3,
						finishTime4: finishTime4, finishTime5: finishTime5,  
						distance1: distance1, distance2: distance2, distance3: distance3,
						distance4: distance4, distance5: distance5}, 
						function(result){ 
							$("#result-div").html(result); }
				); //$.post
		} else {
			$("#result-div").html("<span class='red'>Contestant number is required</span>");
		}
    }); //#btnAddRace
	

	
}); //document.ready
