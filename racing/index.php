<?php
		
  //Buffer larger content areas like the main page content
  ob_start();

?>

<div class="content">

	<form>
		<b>Race Date :</b>
		<input type="date" id="dateRace" name="date_picker" value="<?php echo date('Y-m-d'); ?>" >
		<br/><br/>
		
		<b>Venue Description :</b>
		<input type="text" id="txtVenueName" name="venueName" placeholder="e.g. IPSWICH DOGS (QLD)" size="40" maxlength="50" />
		<br/><br/>
		
		<b>Venue Code :</b>
		<input type="text" id="txtVenueCode" name="venueCode" style='text-transform:uppercase'>
		<br><br/>
		
		<b>Race No. :</b>
		<input type="number" id="txtRaceNum" name="raceNumber" min="1" max="30" >
		<br><br/>
		
		<b>Distance :</b>
		<input type="number" id="txtDistance" name="distance" min="50" max="9000" placeholder="metres">
		<br><br/>
	</form> 
	
	<button id="btnAddRace" >Add Race</button>
	
	<br/><br/>
	<div id="result-div">
		
				
	</div>
	
		
</div> <!-- class=content -->

<script type="text/javascript" src="js/add_race.js" ></script>
	
		
		

<?php
	  //Assign all Page Specific variables
	  $MainContent = ob_get_contents();
	  ob_end_clean();
	  $title = "Home";
	  
	  
	  
	  $rightContent = " 
		
	  ";
	  
	  

	  
	  //Apply the template
	  include("masterpage.php");
	?>