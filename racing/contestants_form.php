<?php
	
  //Buffer larger content areas like the main page content
  ob_start();

?>

<div class="content">
	<?php
	if (isset($_GET['race_date']) && 
		isset($_GET['venue_name']) && 
		isset($_GET['venue_code']) && 
		isset($_GET['race_num']) && 
		isset($_GET['distance'])) {
		
		$race_date = $_GET['race_date'];
		$venue_name = $_GET['venue_name'];
		$venue_code = $_GET['venue_code'];
		$race_num = $_GET['race_num'];
		$distance = $_GET['distance'];
		
		include_once "function/function_race_info.php";
		
		$race_id = get_race_id($race_date,$venue_code,$race_num);
		
		echo "
		$race_date <br/>
		<b>$venue_code $venue_name Race $race_num ($distance metres)</b><br/>";
		
	}
	?>
	<br/><a href="race_analysis.php?race_id=<?php echo$race_id ?>" >Race Analysis</a>
	<br/>
	<form>
		<input type="hidden" id='hiddenRaceID' name='raceID' value='<?php echo $race_id ?>' >
		<b>Contestant No. :</b>
		<input type="number" id="txtContestantNum" name="contestantNum" min="1" max="30" >
		
		&nbsp &nbsp &nbsp
		
		<b>Contestant Name :</b>
		<input type="text" id="txtName" name="name">
		<br><br/>
		
		
		<b>Finishing time of last five races (from most recent to least recent) :</b>
		<br><br/>
		Time (s): <input type="number" id="txtFinishTime1" name="FinishTime1" placeholder="seconds" size="5" step=".01"> &nbsp &nbsp
		Distance (m): <input type="number" id="txtDistance1" name="Distance1" placeholder="distance" value="<?php echo $distance; ?>">
		<br><br/>
		Time (s): <input type="number" id="txtFinishTime2" name="FinishTime2" placeholder="seconds" step=".01"> &nbsp &nbsp
		Distance (m): <input type="number" id="txtDistance2" name="Distance2" placeholder="distance" value="<?php echo $distance; ?>">
		<br><br/>
		Time (s): <input type="number" id="txtFinishTime3" name="FinishTime3" placeholder="seconds" step=".01"> &nbsp &nbsp
		Distance (m): <input type="number" id="txtDistance3" name="Distance3" placeholder="distance" value="<?php echo $distance; ?>">
		<br><br/>
		Time (s): <input type="number" id="txtFinishTime4" name="FinishTime4" placeholder="seconds" step=".01"> &nbsp &nbsp
		Distance (m): <input type="number" id="txtDistance4" name="Distance4" placeholder="distance" value="<?php echo $distance; ?>">
		<br><br/>
		Time (s): <input type="number" id="txtFinishTime5" name="FinishTime5" placeholder="seconds" step=".01"> &nbsp &nbsp
		Distance (m): <input type="number" id="txtDistance5" name="Distance5" placeholder="distance" value="<?php echo $distance; ?>">
		<br><br/>
		
		
	</form> 
	
	<button id="btnAddContestant" >Add Contestant</button>
	<br/><br/>
	<div id="result-div"></div>
	
	<hr/>
	<p>OR Upload XML file of contestant data.</p> 
	
	<form id="upload_form" enctype="multipart/form-data" method="post">
		<input type="button" id="btnUpload" value="Upload XML file" onclick="document.getElementById('file1').click();" />
		<input type="file" name="file1" id="file1" accept="z*/*" style="display:none;">
		<br>
		<progress id="progressBar" value="0" max="100" style="width:300px;display:none;"></progress>
		<b id="status"></b>
		<p id="loaded_n_total"></p>
	</form>
	
	
	
	
	
		
</div> <!-- class=content -->

<script type="text/javascript" src="js/add_contestant.js" ></script>
<script type="text/javascript" src="js/upload-script.js" ></script>
	
		
		

<?php
	  //Assign all Page Specific variables
	  $MainContent = ob_get_contents();
	  ob_end_clean();
	  $title = "Add Contestants";
	  
	  
	  
	  $rightContent = " 
		
	  ";
	  
	  

	  
	  //Apply the template
	  include("masterpage.php");
	?>