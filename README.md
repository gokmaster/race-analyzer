# Racing

A web application written in PHP that sorts the contestants of a greyhound race according various factors such as their best speed, average speed, etc.

# How to run this web application on your server?
1. Place the contents of the racing folder in the root directory of your server such as in "htdocs" folder if your running XAMPP, or in "www" folder if your running WAMP.

2. Import the sql file located in the racing_db folder to your datebase.
